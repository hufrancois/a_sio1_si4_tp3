package tp3;

import java.util.Random;
import java.util.Scanner;

public class TP3exo2 {

    public static void main(String[] args) {
        
        Scanner clavier = new Scanner(System.in);
        
        Random alea= new Random();
        int nbrand = alea.nextInt(1000)+1, nbr, stop=0;
        
        System.out.println("Entrez un nombre entre 1 et 1000 : ");
        
        
        while(stop == 0) {
            nbr = clavier.nextInt();
            
            if(nbr > nbrand) {
                System.out.println("Le nombre à trouver est plus petit");
            }
            
            else if(nbr < nbrand) {
                System.out.println("Le nombre à trouver est plus grand");
            }
            
            else {
                stop = 1;
            } 
        }
        
        
        
        System.out.println("Le nombre à été trouvé.");    
    }
    
    
}
