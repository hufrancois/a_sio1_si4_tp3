package tp3;

import java.util.Random;
import java.util.Scanner;

public class TP3exo4 {

    public static void main(String[] args) {
        
        Scanner clavier = new Scanner(System.in);
        
        Random alea = new Random();
        int nbrand = alea.nextInt(1000)+1;
        int nbr=0;
        int stop=0;
        int nbessai=0;
        long tpsDebut = System.currentTimeMillis();
        
        System.out.println("Entrez un nombre entre 1 et 1000 ou 0 pour abandonner :");
        
        while(stop == 0) {
            nbr = clavier.nextInt();
            
            if(nbr == 0) { 
                System.out.println("Vous avez entré le nombre 0 ,donc vous abandonner. Le nombre à trouvé était "+nbrand);
                stop=1;
                
            }
            
            else {
                nbessai++;
                
                if(nbr > nbrand) {
                    System.out.println("Le nombre à trouver est plus petit");
                    
                }
                else if(nbr < nbrand) {
                    System.out.println("Le nombre à trouver est plus grand");
                    
                }
                else {
                    long tpsFin = System.currentTimeMillis();
                    float tpseconds = (tpsFin - tpsDebut) / 1000;
                    
                    System.out.println("Le bon nombre à été trouvé en "+ nbessai +" essais en "+ tpseconds +"seconds.");
                    stop = 1;
                    
                } 
            }
            
        }
        
        
        
        
        
    }
}
