package tp3;

import java.util.Scanner;

public class TP3exo1 {

    public static void main(String[] args) {
        
        Scanner clavier = new Scanner(System.in);
        
        int nbnote=0, stop=0;
        float max=0, min=20, add=0, note=0;
        
        while(stop == 0) {
            System.out.println("Entrez une note : ");
            note = clavier.nextFloat();
            
            if(note > -1) { add = (float) (add + note); }
            
            if(note > max) { max = (float) note; }
            
            if(note > -1 && note < min) { min = (float) note; }
            
            if(note <= -1) {
                stop = 1;
            }
            
            else {
                nbnote++;
            }
        }
        
        System.out.println("Le nombre de note est de : " + nbnote);
        
        System.out.println("La moyenne des notes est de " + add/nbnote + ".");
        
        System.out.println("La note minimale est " + min + " et la maximale est "+ max);  
    }
}
